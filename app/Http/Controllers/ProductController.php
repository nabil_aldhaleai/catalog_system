<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductResourceCollection;

class ProductController extends Controller
{
    public function show(Product $product): ProductResource
    {
        return new ProductResource($product);
    }

    public function index(): ProductResourceCollection
    {
        return new ProductResourceCollection(Product::paginate());
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'sku'   => 'required',
            'price' => 'required' 
        ]);

        $product = Product::create($request->all());

        return new ProductResource($product);

    }

    public function update(Product $product, Request $request): ProductResource
    {
        $request->validate([
            'name'  => 'string',
            'sku'   => 'string',
            'price' => 'number' 
        ]);

        $product->update($request->all());

        return new ProductResource($product);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        
        return response()->json();
    }
}
